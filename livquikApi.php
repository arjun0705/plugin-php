<?php
/*
	LivQuik Api Curl Base Library.
	Makes cURL call to the LivQuik api routes.

	@company LivQuik Technology
	@license: http://opensource.org/licenses/mit-license.html MIT
	@version: 0.0.1
	@author: Ashish Ojha <ashish.ojha@livquik.com>
	@copyright: 2015-2016, 2015 Ashish Ojha
*/

ini_set('ERRORS_ALL', 1);

// Preparing cURL Data
class CurlCall {
	
	// @var curl resource
	protected $ch = null;

	// @var options to set for curl call
	protected $options = null;
	
	// @var response returned from server
	protected $response = null;

	// We will initiate curl here
	// The constructor
	function __construct() {
		$this->ch = curl_init();
	}
	
	// The destructor
	function __destruct() {
		if(is_resource($this->ch))
			curl_close($this->ch);
		$this->ch = null;
	}

	// Sets default options for the curl call
	public function setDefaultOptions() {
		$this->options = array(
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER         => false,
      CURLOPT_ENCODING       => 'gzip,deflate',
      CURLOPT_CONNECTTIMEOUT => 15,
      CURLOPT_TIMEOUT        => 30,
			CURLOPT_SSL_VERIFYPEER => false,
// These may be required depending on the cURL configuration
			//CURLOPT_SSL_VERIFYHOST => 1,
			//CURLOPT_CAINFO => "../certs/cacert.pem",
			//CURLOPT_HTTPHEADER => array('Host: uat.quikpay.in'),
        	);
	}

	// Post call
	// @param url: takes the url to call to
	// @param $requestParams: extra params data to send with request
	public function post($url, $reqParams) {
		$this->setDefaultOptions();
		curl_setopt($this->ch, CURLOPT_URL, $url);
		curl_setopt_array($this->ch, $this->options);
		curl_setopt($this->ch, CURLOPT_POST, 1);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, $reqParams);

		return $this->request();
	}

	// Executes the curl request
	public function request() {
		$this->response = curl_exec($this->ch);
		if ($this->response === false) {
			$this->response = curl_error($this->ch);
		}

		return $this->response;
	}

}

// Example: 
// How to use:

// Read Config
$config_directory_folder = "../config";
$config = parse_ini_file($config_directory_folder."/livquik.config.ini", true);

// Getting data to build url
$proto = $config['livquik']['proto'];
$domain = $config['livquik']['host'];
$api = $config['livquik']['api'];
$type = $config['livquik']['type'];
$partnerid = $config['livquik']['partnerid'];
$path = $config['livquik']['path'];

// Url to call
$url = $proto.$domain.$api.$type.$partnerid.$path;

// Example data to post, postFields
// Replace the values with your specific ones
$postFields = Array( 
	"partnerid" => "2",
	"outletid" => "39",
	"mobile" => "9833912110",
	"secret" => "oXPpwQRrUL9WSaA0K0aE240k46gR867H",
	"amount" => "100",
	"orderid" => "BBC12133",
	"redirecturl" => "http://localhost/config/livquik.config.ini",
);

// Building post data
$postFields = http_build_query($postFields);

// Initiate object
$curl = new CurlCall;

// Fetching response 
$resp = $curl->post($url, $postFields);

// Decode
$r = json_decode($resp, true);

// Redirecting
header("Location: ". $r['data']['url']);

// Print the response here
//print_r($r);

// Exit Strategy
exit();


?>
